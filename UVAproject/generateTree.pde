ArrayList edges = new ArrayList();
ArrayList visibleEdges = new ArrayList();
boolean firstGenTreeExec = true;

void generateTree()
{
  if (generateNewTree)
  {
    int hierarchyLevelToAffect = 0;
    if (affectSelectedGen)
    {
      hierarchyLevelToAffect = SDR_genViewed.getValueI();
      //println("Affected Generation: " + hierarchyLevelToAffect);
    }
    if (firstGenTreeExec) 
    {
      hierarchyLevelToAffect = 0;
    }
    for (int i = 0; i < subDx; i++)
    {
      for (int j = 0; j < subDy; j++)
      {
        for (int k = 0; k < subDz; k++)
        {
          cells[i][j][k] = new Cell(i, j, k, i * cellSzX, j * cellSzY, k * cellSzZ);
        }
      }
    }
    for (int i = 0; i < subDx; i++)
    {
      for (int j = 0; j < subDy; j++)
      {
        for (int k = 0; k < subDz; k++)
        {
          cells[i][j][k].getNeighbours();
        }
      }
    }
    if (firstGenTreeExec) firstGenTreeExec = false;
    //readDensity();

    if (affectSelectedGen)
    {
      if (seeds.size()>0)
      {
        Iterator it = seeds.iterator();
        while (it.hasNext ()) 
        { 
          Seed s = (Seed)it.next();
          if (s.hierarchyLevel >= hierarchyLevelToAffect) 
          {
            s.childBranches.clear();
            it.remove();
          }
        }
      }
      if (branches.size()>0)
      {
        Iterator it = branches.iterator();
        while (it.hasNext ()) 
        { 
          Branch b = (Branch)it.next();
          if (b.hierarchyLevel == hierarchyLevelToAffect-1) 
          {
            b.seedCount = 0;
            b.childSeeds.clear();
          }
          else if (b.hierarchyLevel >= hierarchyLevelToAffect) 
          {
            //b.childSeeds.clear();
            b.branchCells.clear();
            it.remove();
          }
        }
      }
      for (int i = 0 ; i < branches.size(); i++)
      {
        Branch b = (Branch)branches.get(i);
        for (int j = 0 ; j < b.branchCells.size(); j++)
        {
          Index ind = (Index)b.branchCells.get(j);
          cells[ind.d[0]][ind.d[1]][ind.d[2]].occupied = true;
        }
      }
    }
    else 
    {
      branches.clear();
      seeds.clear();
    }
    
    getSpineCellsByColour();
    println(imgSpineCells.size());
    getCellsBrightness();

    for (int i = hierarchyLevelToAffect; i < generations ; i++)
    {
      if (i==0)
      {
        Seed s = new Seed();
        Cell spSt=(Cell) sortedSpineCells.get(0);
        Index rootIndex = new Index(spSt.ind.d[0],spSt.ind.d[1], 0); //(int(subDx/2), int(subDy/2), 0);
        Index ceilingIndex = new Index(int(subDx/2), int(subDy/2), subDz -1);
        s = new Seed(i, rootIndex, generationStepScale[i][0], generationStepScale[i][1], ceilingIndex);    

        seeds.add(s);
      }
      else
      {
        for (int j = 0 ; j < branches.size(); j++)
        {
          Branch b = (Branch)branches.get(j);
          if (b.hierarchyLevel == i - 1)
          {
            if (i==1) b.sowSeedsOnBranchByBrightness(generationSeeds[i-1][0], generationSeeds[i-1][1], generationStepScale[i][0], generationStepScale[i][1], generationDepthLimits[i][0], generationDepthLimits[i][1]);
            else b.sowSeedsOnBranch(generationSeeds[i-1][0], generationSeeds[i-1][1], generationStepScale[i][0], generationStepScale[i][1], generationDepthLimits[i][0], generationDepthLimits[i][1]);
          }
        }
      }
      boolean hatchGenerationSeeds = true;
      for (int j = 0 ; j < seeds.size(); j++)
      {
        Seed s = (Seed)seeds.get(j);
        if (s.hierarchyLevel == i && s.active)
        {
          s.startGeneratingBranch();
        }
      }
      while (hatchGenerationSeeds)
      {
        int activeSeeds = 0;
        for (int j = 0 ; j < seeds.size(); j++)
        {
          Seed s = (Seed)seeds.get(j);
          if (s.hierarchyLevel == i && s.active)
          {
            activeSeeds++;
            int activeBranches = 0;
            for (int k = 0 ; k < s.childBranches.size(); k++)
            {
              Branch b = (Branch)s.childBranches.get(k);
              if (! (b.reachedGoal ()==true || b.reachedDepthLimit()==true || b.deadEnd==true))
              {
                b.growOneStep();
                activeBranches++;
              }
              else
              {
                s.endGeneratingBranch(b);
              }
            }
            if (activeBranches==0) 
            {
              s.active = false;
            }
          }
        }
        if (activeSeeds==0) 
        {
          hatchGenerationSeeds = false;
        }
      }
    }
//
//    Index[] cellsClosestToAnchors = new Index[anchors.length];
//    for (int n = 0 ; n < anchors.length; n++)
//    {
//      Vec3D anchorVec = new Vec3D(anchors[n].d[0], anchors[n].d[1], anchors[n].d[2]);
//      float minDist = subDz * cellSzZ;
//      Index closestCell = new Index();
//      for (int i = 0; i < subDx; i++)
//      {
//        for (int j = 0; j < subDy; j++)
//        {
//          for (int k = 0; k < subDz; k++)
//          {
//            if (cells[i][j][k].occupied)
//            {
//              Vec3D cellVec = new Vec3D(cells[i][j][k].ind.d[0], cells[i][j][k].ind.d[1], cells[i][j][k].ind.d[2]);
//              float d = anchorVec.distanceTo(cellVec);
//              if (d<minDist) 
//              {
//                minDist = d;
//                closestCell = new Index(i, j, k);
//              }
//            }
//          }
//        }
//      }
//      cellsClosestToAnchors[n] = closestCell;
//    }
//
//    for (int i = 0; i < anchors.length; i++)
//    {
//      Seed s = new Seed(i, anchors[i], generationStepScale[i][0], generationStepScale[i][1], cellsClosestToAnchors[i]); 
//      s.startGeneratingBranch(); 
//      Branch b = (Branch)s.childBranches.get(k);
//      while (! (b.reachedGoal ()==true || b.reachedDepthLimit()==true || b.deadEnd==true))
//      {
//        b.growOneStep();
//      }
//      //      seeds.add(s);
//    }


    edges = new ArrayList();
    visibleEdges= new ArrayList();
    lowestOccupiedInds.clear();

    // edges=new Edge[startIndVec.size()];
    for (int i = 0 ; i < branches.size(); i++)
    {
      Branch b = (Branch)branches.get(i);
      for (int j = 0 ; j < b.branchCells.size()-1; j++)
      {
        Index startPt = (Index)b.branchCells.get(j);
        Index endPt = (Index)b.branchCells.get(j+1);
        Edge e = new Edge(startPt, endPt);
        e.hierarchyLevel = b.hierarchyLevel;
        
        e.visible=int (random(100));

        // println(e.edgeDir.d[0] +","+ e.edgeDir.d[1]+","+ e.edgeDir.d[2]);
        e.LEDfaceDir=new Direction(0, 0, 0);
        float dice=random(100);
        if (dice>50)
        {
          int d=int(random(6));
          //if(d%2==0)
          if ((d%2==0 && e.edgeDir!=directions[d] && e.edgeDir!=directions[d+1]) || (d%2==1 && e.edgeDir!=directions[d] && e.edgeDir!=directions[d-1]) )
          {
            e.LEDfaceDir= directions[d];
          }
        }
        edges.add(e);
        ////select visible edges
        float [] visPercnt={ 20, 50, 20, 40, 40};
        //int [][] selecedAxes=new int[visPercnt.length][3];
        int[][] selecedAxes = {{1,1,1}, {1,1,1}, {0,1,1}, {0,0,1}, {0,0,1}}; 
        int hZones=visPercnt.length;
        //println(hZones);
        
        for (int k=0; k<hZones; k++)
        {
           if(e.visible < visPercnt[k] && e.startInd.d[2]> subDz*k/hZones && e.startInd.d[2]<= subDz*(k+1)/hZones )
          {
            if (selecedAxes[k][0]==1 && (e.edgeDir==directions[0] || e.edgeDir==directions[1]))
            {
              visibleEdges.add(e);
            }
            if (selecedAxes[k][1]==1 && (e.edgeDir==directions[2] || e.edgeDir==directions[3]))
            {
              visibleEdges.add(e);
            }
            if (selecedAxes[k][2]==1 && (e.edgeDir==directions[4] || e.edgeDir==directions[5]))
            {
              visibleEdges.add(e);
            }
          }
        }


//        if (e.visible < 50) 
//          if (e.hierarchyLevel==0)
//         {
//         visibleEdges.add(e);
//          }
//         if(e.visible < 40 && e.startInd.d[2]> subDz*4/5 && (e.edgeDir==directions[4] || e.edgeDir==directions[5]))
//          {
//          visibleEdges.add(e);
//          }
//          else if(e.visible < 40 && e.startInd.d[2]<= subDz*4/5 && e.startInd.d[2]> subDz*3/5 && (e.edgeDir!=directions[0] && e.edgeDir!=directions[1]))
//          {
//          visibleEdges.add(e);
//          }
//          else if(e.visible < 20 && e.startInd.d[2]<= subDz*3/5 && e.startInd.d[2]> subDz*2/5)
//          {
//          visibleEdges.add(e);
//          }
//           else if(e.visible < 50 && e.startInd.d[2]<= subDz*2/5 && e.startInd.d[2]> subDz*1/5)
//          {
//          visibleEdges.add(e);
//          }
//          else if(e.visible < 20 && e.startInd.d[2]<= subDz*1/5 )
//          {
//          visibleEdges.add(e);
//          }
       // }
      
      }
    }
    for (int i=0; i<edges.size(); i++)
    { 
      int s=0;
      int e=0;
      Edge e1 = (Edge)edges.get(i);
      for (int j=0; j<edges.size(); j++)
      {
        if (i!=j)
        {
          Edge e2 = (Edge)edges.get(j);
          e1.getAligned(e2);
          if (e1.sAligned==true) s=1;
          if (e1.eAligned==true) e=1;
        }
      }
      if (s==1)e1.sAligned=true;
      if (e==1)e1.eAligned=true;
    }
    
    
    totalEdgeLength=0;
    totalExtEdgeLength=0;
    // rEdges=new int[edges.size()];
    for (int i=0; i<edges.size(); i++)
    {
      Edge e = (Edge)edges.get(i);
      if (!dispExtensions) totalEdgeLength+= e.getEdgeLength();
      e.getExtensions(); // cross node or simple node
      totalExtEdgeLength+= e.getEdgeLength();
      //println(edges[i].eAligned);
        
    
    
     // rEdges[i]=int(random(100));
    
    }
    ///////////////visible edges
    for (int i=0; i<visibleEdges.size(); i++)
    { 
      int s=0;
      int e=0;
      Edge e1 = (Edge)visibleEdges.get(i);
      for (int j=0; j<visibleEdges.size(); j++)
      {
        if (i!=j)
        {
          Edge e2 = (Edge)visibleEdges.get(j);
          e1.getAligned(e2);
          if (e1.sAligned==true) s=1;
          if (e1.eAligned==true) e=1;
        }
      }
      if (s==1)e1.sAligned=true;
      if (e==1)e1.eAligned=true;
    }
    
    for (int i=0; i<visibleEdges.size(); i++)
    {
      Edge e = (Edge)visibleEdges.get(i);
      e.getExtensions(); // cross node or simple node
      totalVisibleEdgeLength+=e.getEdgeLength();
    }
    ///////////////
    generateNewTree = false;
    longSupportCables();
  }
}


//void readDensity()
//{
//  //readImageBrigtness();
//  float [][][] densityVal=new float[subDx][subDy][subDz];
//s
//  for (int i = 0; i < subDx; i++)
//  {
//    for (int j = 0; j < subDy; j++)
//    {
//      for (int k = 0; k < subDz; k++)
//      {
//        
//       // cells[i][j][k].brVal=;
//      }
//    }
//  }
//  //return(densityVal[cellInd[0]][cellInd[1]][cellInd[2]]);
//}
