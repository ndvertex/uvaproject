  ArrayList imgSpineCells=new ArrayList();
  ArrayList sortedSpineCells=new ArrayList();
  
void getCellsBrightness()
{
  //  PImage img;

  int subdivX=subDx;
  int subdivY=subDz;
  float sum;
  float br;
  float av;
  int stepX;
  int stepY;
  float [][] avBrightness=new float [subdivX][subdivY];

 // img.resize(390,2915);
  img.loadPixels();

 // stepX= int(cellSzX);
 // stepY= int(cellSzZ);
  stepX= int(img.width/subdivX);
  stepY= int(img.height/subdivY);


  for (int i=0; i<subdivX; i++) //subDx
  {
    for (int j=0; j<subdivY; j++)//subDz
    {
      PImage cell=img.get(i*stepX, j*stepY, stepX, stepY);
      cell.loadPixels();
      sum=0;
      for (int k=0; k<cell.pixels.length; k++)
      {
        br=brightness(cell.pixels[k]);
        sum+=br;
      }
      av=sum/(stepX*stepY);
      avBrightness[i][j]=av;

      for (int k = 0; k < subDy; k++)
      {
        cells[i][k][j].brVal= avBrightness[i][j];
      }
    }
  }
}

void getSpineCellsByColour()
{
  

  int subdivX=subDx;
  int subdivY=subDz;
  //float sum;
  //float redVal;
  float br;
  int stepX;
  int stepY;
  float [][] blackCells=new float [subdivX][subdivY];

  spineImg.loadPixels();

 
  stepX= int(spineImg.width/subdivX);
  stepY= int(spineImg.height/subdivY);


  for (int i=0; i<subdivX; i++) //subDx
  {
    for (int j=0; j<subdivY; j++)//subDz
    {
      PImage cell=spineImg.get(i*stepX, j*stepY, stepX, stepY);
      cell.loadPixels();
     // sum=0;
      for (int k=0; k<cell.pixels.length; k++)
      {
       br=brightness(cell.pixels[k]);
       if (br<100)
       {
         blackCells[i][j]=1;
         break;
       }
       //else blackCells[i][j]=0;
      }

      if(blackCells[i][j]==1)
      {
      //for (int k = 0; k < subDy; k++)
      //{
        Cell c=cells[i][0][j];
        imgSpineCells.add(c);
        
      //}
      }
    }
  }
  for (int i=0; i< imgSpineCells.size(); i++)//the sorting is not working!!!
  {
    Cell c=(Cell) imgSpineCells.get(i);
    
    if (i==0) sortedSpineCells.add(c);
    if (i>0)
    {
      Cell cFirst=(Cell) sortedSpineCells.get(0);
      Cell cLast=(Cell) sortedSpineCells.get(sortedSpineCells.size()-1);
      if (c.ind.d[2] < cFirst.ind.d[2])
      {
        sortedSpineCells.add(0,c);
      }
      else if (c.ind.d[2] > cLast.ind.d[2])
      {
       sortedSpineCells.add(c);
      }
    }
  }
}
