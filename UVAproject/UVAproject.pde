//////////////////////////////////////////////////////////////
// U V A 
// W e l l c o m e   T r u s t   
// 
// Boundary Dimensions: 1.55 x 3.90 x 29.15
// * * U N I T S   A R E   I N   c m * *
//////////////////////////////////////////////////////////////

import toxi.geom.*;
import peasy.org.apache.commons.math.*;
import peasy.*;
import peasy.org.apache.commons.math.geometry.*;
import saito.objloader.*;
import processing.dxf.*;
import g4p_controls.*;
import java.awt.Font;
import java.awt.*;
import java.util.*;

void setup()
{
  size(1024, 1024, P3D);
  createGUI();
  cam = new PeasyCam(this, centre.getX(), centre.getY(), centre.getZ(), 2000);
  cam.setState(frontView, 0);
  cam.setMinimumDistance(0.1);
  cam.setMaximumDistance(10000);
  cam.setWheelScale(3.5);

  directions[0] = new Direction(1, 0, 0);
  directions[1] = new Direction(-1, 0, 0);
  directions[2] = new Direction(0, 1, 0);
  directions[3] = new Direction(0, -1, 0);
  directions[4] = new Direction(0, 0, 1);
  directions[5] = new Direction(0, 0, -1);

  anchors[0]=new Index(0, int(subDy/3), int(subDz/6));
  anchors[1]=new Index(subDx-1, int(subDy*2/3), int(subDz/3));
  anchors[2]=new Index(int(subDx/2), 0, int(subDz/2));
  anchors[3]=new Index(int(subDx/2), subDy-1, int(subDz*2/3));
  anchors[4]=new Index(0, subDy-1, int(subDz*5/6));



  font = loadFont("PFIsotextPro-Regular-48.vlw");
  textAlign(LEFT, TOP);

  model = new OBJModel(this, "modelCentered.obj", QUADS);
  //model.enableDebug();
  //model.scale(1);
  //model.translateToCenter();

  // println(subDz);
  //floorLvl[0]=refLvl/100;
  for (int i = 0; i < floorLvl.length; i++)
  {
    floorLvl[i] *= 100f;
    //floorLvl[i] -=refLvl;
    // subDzPerFloor[i] = int((floorLvl[i]-refLvl)/cellSzZ); //refLvl
    //println(subDzPerFloor[i]);
  }
  // subDzPerFloor=subset(subDzPerFloor, 1);
  //  println(subDzPerFloor);

  perspective(PI/1.8f, float(width)/float(height), ((float(height)/2.0) / tan(PI/6f)) / 10000f, ((float(height)/2.0) / tan(PI/6f)) * 100f);
  //smooth(16);
  noSmooth();

  rectMode(CENTER);
  // generateTree();
  img=loadImage("WTstaircase_gradient_04invert.jpg");
  spineImg=loadImage("WTstaircase_spineLnBlck.jpg");
}

void draw()
{

  generateTree();
  //supportCables();
  
  if (recordDXF) {
    beginRaw(DXF, "output-###.dxf");
  }

  //println(cam.getDistance());
  handleView();
  //Vec3D lightNormal = new Vec3D(cam.getPosition()[0], cam.getPosition()[1], cam.getPosition()[2]);
  //lightNormal.subSelf(cam.getLookAt()[0], cam.getLookAt()[1],cam.getLookAt()[2]);
  //lightNormal.normalize();
  //lightNormal.rotateAroundAxis(new Vec3D(cam.,)
  background(255);
  //directionalLight(126, 126, 126, -cam.getRotations()[0], -cam.getRotations()[1], -cam.getRotations()[2]);
  //directionalLight(126, 126, 126, lightNormal.x, lightNormal.y, lightNormal.z);
  // ambientLight(102, 102, 102);
  //lights();
  if (dispImage) 
  {
    pushMatrix();
    rotateX(PI/2);
    //scale(390/img.width, 2915/img.height);
    image(img, 0, 0, 390, 2915);
    popMatrix();
  }
  
   for (int i=0; i<sortedSpineCells.size(); i++)
   {
     Cell c= (Cell)sortedSpineCells.get(1);
     c.draw(color(255, 0, 0), 5);
   }
   
  // blendMode(BLEND);
  //lights();
  if (dispUCS)
  {
    strokeWeight(1);
    stroke(255, 0, 0);
    line(0, 0, 0, 4000, 0, 0);
    stroke(0, 255, 0);
    line(0, 0, 0, 0, 4000, 0);
    stroke(0, 0, 255);
    line(0, 0, 0, 0, 0, 4000);
  }

  //strokeWeight(0.5);
  //stroke(128);
  //fill(255, 255, 0);
  if (dispNodes)
  {
    for (int i = 0; i < subDx; i++)
    {
      for (int j = 0; j < subDy; j++)
      {
        for (int k = 0; k < subDz; k++)
        {
          if (cells[i][j][k].occupied) cells[i][j][k].draw(color(255, 0, 0), 5);
          //  cells[i][j][k].draw(color(255, 0, 0), 5);
        }
      }
    }
  }

  // blendMode(ADD);
  blendMode(MULTIPLY);
  strokeWeight(0.5);
  //stroke(255, 255, 0, 92);
  stroke(92, 32);
  noFill();

  if (dispSuspensionCables) drawCables();
  if (dispGrid) renderGridLines();
  blendMode(NORMAL);
  stroke(150, 128);
  strokeWeight(3);

  if (dispSlabs)
  {
    for (int i = 0; i < floorLvl.length; i++)
    {
      pushMatrix();
      translate((float)(centre.getX()), (float)(centre.getY()), floorLvl[i] - refLvl);
      rect(0, 0, slabSz, slabSz);
      translate(0, 0, -slabThickness);
      rect(0, 0, slabSz, slabSz);
      popMatrix();
    }
  }
  if (dispExtensions)
  {
    stroke(0);
    strokeWeight(1);
    for (int i=0; i<visibleEdges.size(); i++)
    {
      Edge e = (Edge)visibleEdges.get(i);
      e.drawEdge();
    }

    //    for (int i=0; i<edges.size(); i++)
    //    {
    //     // int r=int(random(2));
    //     
    //      Edge e = (Edge)edges.get(i);
    // strokeWeight(0.5);
    //stroke(0);
    ////      if (e.LEDfaceDir.d[0]==0 && e.LEDfaceDir.d[1]==0 && e.LEDfaceDir.d[2]==0) stroke(0);
    ////      else if (e.LEDfaceDir== directions[0]) stroke(255, 255, 0);
    ////      else if (e.LEDfaceDir== directions[1]) stroke(255, 0, 0);
    ////      else if (e.LEDfaceDir== directions[2]) stroke(0, 255, 0);
    ////      else if (e.LEDfaceDir== directions[3]) stroke(0, 0, 255);
    ////      else if (e.LEDfaceDir== directions[4]) stroke(0, 255, 255);
    ////      else if (e.LEDfaceDir== directions[5]) stroke(255, 0, 255);
    //
    //    // if (rEdges[i]>60) 
    //   // if (e.visible==1)
    //   // {
    //     e.drawEdge();
    //   // }
    //    }
  }
  else 
  {
    for (int i = 0 ; i < branches.size(); i++)
    {
      Branch b = (Branch)branches.get(i);
      b.draw();
    }
  }

  if (dispBoxes)
  {
    strokeWeight(0.5);
    for (int i=0; i<visibleEdges.size(); i++)
    {
      Edge e = (Edge)visibleEdges.get(i);

      Vec3D sPt=new Vec3D(e.sX, e.sY, e.sZ);
      Vec3D ePt=new Vec3D(e.eX, e.eY, e.eZ);
      Vec3D trans= ePt.copy();
      trans.subSelf(sPt);
      trans.scaleSelf(0.5);
      trans.addSelf(sPt);

      pushMatrix();
      //translate(trans.x,trans.y, trans.z);
      // strokeWeight(8);
      translate(trans.x, trans.y, trans.z );

      // strokeWeight(1);
      stroke(0);
      fill(100);
      if (e.edgeDir==directions[0]||e.edgeDir==directions[1]) 
      {
        box(abs(e.eX-e.sX), strutTh, strutTh);
        if (!(e.LEDfaceDir.d[0]==0 && e.LEDfaceDir.d[1]==0 && e.LEDfaceDir.d[2]==0))//&& !(e.LEDfaceDir==directions[0] || e.LEDfaceDir==directions[1]))
        {
          if (e.LEDfaceDir==directions[2])
          {
            translate(0, strutTh/2, 0);
            rotateX(PI/2);
          }
          else if (e.LEDfaceDir==directions[3])
          {
            translate(0, -strutTh/2, 0);
            rotateX(PI/2);
          }
          else if (e.LEDfaceDir==directions[4])
          {
            translate(0, 0, strutTh/2);
            //rotateX(PI/2);
          }
          else if (e.LEDfaceDir==directions[5])
          {
            translate(0, 0, -strutTh/2);
            //rotateX(PI/2);
          }
          fill(255, 255, 0);
          rect(0, 0, abs(e.eX-e.sX), strutTh);
          //noFill();
        }
      }
      if (e.edgeDir==directions[2] || e.edgeDir==directions[3]) 
      {
        fill(100);
        box(strutTh, abs(e.eY-e.sY), strutTh);
        if (!(e.LEDfaceDir.d[0]==0 && e.LEDfaceDir.d[1]==0 && e.LEDfaceDir.d[2]==0))//&& !(e.LEDfaceDir==directions[2] || e.LEDfaceDir==directions[3]))
        {
          if (e.LEDfaceDir==directions[0])
          {
            translate(strutTh/2, 0, 0 );
            rotateY(PI/2);
          }
          else if (e.LEDfaceDir==directions[1])
          {
            translate(-strutTh/2, 0, 0);
            rotateY(PI/2);
          }
          else if (e.LEDfaceDir==directions[4])
          {
            translate(0, 0, strutTh/2);
            //rotateX(PI/2);
          }
          else if (e.LEDfaceDir==directions[5])
          {
            translate(0, 0, -strutTh/2);
            //rotateX(PI/2);
          }
          fill(255, 255, 0);
          rect(0, 0, strutTh, abs(e.eY-e.sY));
          noFill();
        }
      }
      if (e.edgeDir==directions[4] || e.edgeDir==directions[5]) 
      {
        fill(100);
        box(strutTh, strutTh, abs(e.eZ-e.sZ));
        if (!(e.LEDfaceDir.d[0]==0 && e.LEDfaceDir.d[1]==0 && e.LEDfaceDir.d[2]==0))// && !(e.LEDfaceDir==directions[4] || e.LEDfaceDir==directions[5]))
        {
          if (e.LEDfaceDir==directions[0])
          {
            translate(strutTh/2, 0, 0 );
            rotateY(PI/2);
          }
          else if (e.LEDfaceDir==directions[1])
          {
            translate(-strutTh/2, 0, 0);
            rotateY(PI/2);
          }
          else if (e.LEDfaceDir==directions[2])
          {
            rotateZ(PI/2);
            translate(strutTh/2, 0, 0);
            rotateY(PI/2);
          }
          else if (e.LEDfaceDir==directions[3])
          {
            rotateZ(PI/2);
            translate( -strutTh/2, 0, 0);
            rotateY(PI/2);
          }
          fill(255, 255, 0);
          rect(0, 0, abs(e.eZ-e.sZ), strutTh);
          noFill();
        }
      }
      popMatrix();
    }
  }

  if (dispModel)
  {
    strokeWeight(1);
    rotateX(-PI/2);
    stroke(200, 150);
    pushMatrix();
    translate((float)(centre.getX()), 0, (float)(centre.getY()));//(subDx/2f-0.5f)*cellSzX, 0, (subDy/2f-0.5f)*cellSzY);
    model.draw();
    popMatrix();
  }
  cam.beginHUD();
  ortho();
  fill(0);
  textFont(font, 18);
  float txtTransX=10;
  float txtTransY=10;
  //text("generations: " + generations, txtTransX, txtTransY);
  //    text("number of nodes: " + nodeCount, txtTransX, txtTransY+30);
  text("grid size (cm): x=" + int(cellSzX) +", y="+ int(cellSzY) +", z="+ int(cellSzZ), txtTransX, txtTransY);
  text("number of edges: " + edges.size(), txtTransX, txtTransY+ 30);
  text("number of visible edges: " + visibleEdges.size() +" ("+ (100* visibleEdges.size()/edges.size()) +"%)", txtTransX, txtTransY+60);
  //  if (!dispExtensions) text("total length: " +totalEdgeLength, txtTransX, txtTransY+ 120);
  //  else text("total length: " +totalExtEdgeLength, txtTransX, txtTransY+ 120);
  text("total length: " +totalEdgeLength +"/total ext. length: " +totalExtEdgeLength, txtTransX, txtTransY+ 90);
  text("total visible edges length: " +totalVisibleEdgeLength, txtTransX, txtTransY+ 120);
  text("number of cables: " +lowestOccupiedInds.size(), txtTransX, txtTransY+ 150);
  text("generationSeeds: " + generationSeeds[0][0] +"-"+ generationSeeds[0][1] +", "+ generationSeeds[1][0] +"-"+ generationSeeds[1][1] +", "+ generationSeeds[2][0] +"-"+ generationSeeds[2][1] +", "+ generationSeeds[3][0] +"-"+ generationSeeds[3][1] +", "+ generationSeeds[4][0] +"-"+ generationSeeds[4][1], txtTransX, txtTransY+ 180);
  text("generationStepScale: " + generationStepScale[0][0] +"-"+ generationStepScale[0][1] +", "+ generationStepScale[1][0] +"-"+ generationStepScale[1][1] +", "+ generationStepScale[2][0] +"-"+ generationStepScale[2][1] +", "+ generationStepScale[3][0] +"-"+ generationStepScale[3][1] +", "+ generationStepScale[4][0] +"-"+ generationStepScale[4][1], txtTransX, txtTransY+ 210);
  text("generationDepthLimits: " + generationDepthLimits[0][0] +"-"+ generationDepthLimits[0][1] +", "+ generationDepthLimits[1][0] +"-"+ generationDepthLimits[1][1] +", "+ generationDepthLimits[2][0] +"-"+ generationDepthLimits[2][1] +", "+ generationDepthLimits[3][0] +"-"+ generationDepthLimits[3][1] +", "+ generationDepthLimits[4][0] +"-"+ generationDepthLimits[4][1], txtTransX, txtTransY+ 240);
  text("seed distribution by brightness (%): " + distrPrcnt[0] +", "+distrPrcnt[1] +", "+distrPrcnt[2] +", "+distrPrcnt[3] +", "+distrPrcnt[4], txtTransX, txtTransY+ 270);
  
  cam.endHUD();

  if (recordDXF) {
    endRaw();
    recordDXF = false;
  }
}


void handleView()
{

  if (view!=0)
  {
    if (view==1)
    {
      cam.setState(topView);
    }
    if (view==2)
    {
      cam.setState(frontView);
    }
    else if (view==3)
    {
      cam.setState(leftView);
    }
    view = 0;
  }
  if (orthoMode) 
  {
    float leftClip = - (float(width)/float(height)) * ((float)cam.getDistance()) + float(height) * 0.5f;
    float rightClip = (float(width)/float(height)) * ((float)cam.getDistance()) + float(height) * 0.5f;
    float bottomClip = - (float)cam.getDistance() + float(height) * 0.5f;
    float topClip = (float)cam.getDistance() + float(height) * 0.5f;
    ortho(leftClip, rightClip, bottomClip, topClip, -20000, 20000);
  }
  else
  {
    perspective(PI/2f, float(width)/float(height), ((float(height)/2.0) / tan(PI/6f)) / 10000f, ((float(height)/2.0) / tan(PI/6f)) * 100f);
  }
}

void renderGridLines()
{
  for (int i = 0; i < subDx+1; i++)
  {
    for (int j = 0; j < subDy+1; j++)
    {
      line((i-0.5f)*cellSzX, (j-0.5f)*cellSzY, -0.5f*cellSzZ, (i-0.5f)*cellSzX, (j-0.5f)*cellSzY, (subDz-0.5f)*cellSzZ);
    }
  }
  //  for (int i = 0; i < subDx+1; i++)
  //  {
  //    for (int j = 0; j < subDz+1; j++)
  //    {
  //      line((i-0.5f)*cellSzX, -0.5f*cellSzY, (j-0.5f)*cellSzZ, (i-0.5f)*cellSzX, (subDy-0.5f)*cellSzY, (j-0.5f)*cellSzZ);
  //    }
  //  }
  //  for (int i = 0; i < subDy+1; i++)
  //  {
  //    for (int j = 0; j < subDz+1; j++)
  //    {
  //      line(-0.5f*cellSzX, (i-0.5f)*cellSzY, (j-0.5f)*cellSzZ, (subDx-0.5f)*cellSzX, (i-0.5f)*cellSzY, (j-0.5f)*cellSzZ);
  //    }
  //  }
}

void keyReleased() 
{
  if (key == '0') 
  { 
    view = 0;
  }
  if (key == '1') 
  {
    view = 1;
  }
  if (key == '2') 
  {
    view = 2;
  }
  if (key == '3') 
  {
    view = 3;
  }
  if (key == 'o' || key == 'O') orthoMode = true;
  if (key == 'p' || key == 'P') 
  {
    perspective(PI/2f, float(width)/float(height), ((float(height)/2.0) / tan(PI/6f)) / 10000f, ((float(height)/2.0) / tan(PI/6f)) * 100f);
    orthoMode = false;
  }

  // if (key == 'w' || key == 'W') white = true;
  //  if (key == 'c' || key == 'C') white = false;

  if (key == 'n' || key == 'N') 
  {

    counter++;
    if (counter%2==0)dispNodes = true;
    else dispNodes = false;
  }
  if (key == 'w' || key == 'W') 
  {
    counterB++;
    if (counterB%2==0)dispUCS= true;
    else dispUCS=  false;
  }
  if (key == 'g' || key == 'G') 
  {
    counterC++;
    if (counterC%2==0)dispGrid= true;
    else dispGrid=  false;
  }
  if (key == 's' || key == 'S') 
  {
    counterD++;
    if (counterD%2==0)dispSlabs= true;
    else dispSlabs=  false;
  }
  if (key == 'm' || key == 'M') 
  {
    counterE++;
    if (counterE%2==0)dispModel= true;
    else dispModel=  false;
  }
  if (key == 'r'|| key == 'R') 
  {
    recordDXF = true;
  }
  if (key == 'i'|| key == 'I') 
  {
    saveFrame("D:/WellcomeTrust/Anna/scripts/screenshots_firstSketches/LSystems_Spine-###.jpg");
  }
  //  if (key == CODED && keyCode == UP) 
  //  {
  //    indX++;
  //  }
  if (key == 'b' || key == 'B') 
  {
    counterE++;
    if (counterE%2==0)dispBoxes= true;
    else dispBoxes=  false;
  }
}
