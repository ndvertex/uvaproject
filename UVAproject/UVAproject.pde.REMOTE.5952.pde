

//////////////////////////////////////////////////////////////
// U V A 
// W e l l c o m e   T r u s t   
// 
// Boundary Dimensions: 1.55 x 3.90 x 29.15
// * * U N I T S   A R E   I N   c m * *
//////////////////////////////////////////////////////////////

import toxi.geom.*;

import peasy.org.apache.commons.math.*;
import peasy.*;
import peasy.org.apache.commons.math.geometry.*;

import saito.objloader.*;
import g4p_controls.*;

int subDx = 12;
int subDy = 6;
int subDz = 15;//95;
float cellSzX = 100 * 0.3;
float cellSzY = 100 * 0.3;
float cellSzZ = 100 * 0.5;

Cell[][][] cells = new Cell[subDx][subDy][subDz];

PeasyCam cam;
Vector3D centre = new Vector3D(0.5f * cellSzX * (subDx - 1), 0.5f * cellSzY * (subDy - 1), 0.5f * cellSzZ * (subDz - 1));
CameraState topView = new CameraState(new Rotation(new Vector3D(0, 0, 1), 0), centre, 2000);
CameraState frontView = new CameraState(new Rotation(new Vector3D(1, 0, 0), -PI/2), centre, 2000);
CameraState leftView = new CameraState(new Rotation(RotationOrder.ZXY, -PI/2, -PI/2, 0), centre, 2000);
int view = 0;
boolean orthoMode = false;
boolean showNodes = true;
boolean showUCS = true;

// These units are in meters. They need to be converted to cm (currently done in setup())
float[] floorLvl = {
  19.96, 23.46, 26.96, 32.14, 36.70, 40.98, 44.94, 49.06, 52.44
};
float refLvl = 100 * (2.75 + 19.96); //cm
float slabThickness = 50; //cm
float slabSz = 1000f; //cm

//OBJModel model;
////////////////////////////LSys
int seedNum=8;
Branch [] trees=new Branch[seedNum];
Cell [] seedPts=new Cell[seedNum];
color [] treeCol=new color [seedNum];
int maxDepth;
int branchCount;

ArrayList startIndVec = new ArrayList();
ArrayList endIndVec = new ArrayList();

Edge [] edges;

boolean white;
int counter=0;
int counterB=0;

PFont font;
///////////////////////////////////


void setup()
{
  size(1024, 1024, P3D);
  createGUI();
  cam = new PeasyCam(this, centre.getX(), centre.getY(), centre.getZ(), 2000);
  cam.setState(frontView, 0);
  cam.setMinimumDistance(0.1);
  cam.setMaximumDistance(10000);
  cam.setWheelScale(3.5);

  font = loadFont("PFIsotextPro-Regular-48.vlw");
  //font = createFont("PFIsotextPro-Regular", 48);
  textAlign(LEFT, TOP);
  // model = new OBJModel(this, "model.obj", LINES);
  //model.enableDebug();
  //model.scale(1);
  //model.translateToCenter();

  for (int i = 0; i < floorLvl.length; i++)
  {
    floorLvl[i] *= 100f;
  }

  for (int i = 0; i < subDx; i++)
  {
    for (int j = 0; j < subDy; j++)
    {
      for (int k = 0; k < subDz; k++)
      {
        cells[i][j][k] = new Cell(i, j, k, i * cellSzX, j * cellSzY, k * cellSzZ);
      }
    }
  }
  for (int i = 0; i < subDx; i++)
  {
    for (int j = 0; j < subDy; j++)
    {
      for (int k = 0; k < subDz; k++)
      {
        cells[i][j][k].getNeighbours();
      }
    }
  }

  //perspective(PI*0.35, 1.6, (height/2.0) / tan(PI*60.0/360.0)/160, (height/2.0) / tan(PI*60.0/360.0)*10);
  // perspective(PI/3f, float(width)/float(height), ((float(height)/2.0) / tan(PI/6f)) / 10000f, ((float(height)/2.0) / tan(PI/6f)) * 100f);
  perspective(PI/1.8f, float(width)/float(height), ((float(height)/2.0) / tan(PI/6f)) / 10000f, ((float(height)/2.0) / tan(PI/6f)) * 100f);
  // ortho(float(width)*0.48, float(width)*0.52, float(height)*0.48, float(height)*0.52, -500, 100);
  // ortho(float(width)/float(height) * (float)-cam.getDistance(), float(width)/float(height) * (float)cam.getDistance(), (float)-cam.getDistance(), (float)cam.getDistance(), -1000, 1000);
  //smooth(8);
  // noSmooth();

  rectMode(CENTER);
  /////////////////////////LSys START
  // color [] treeCol=new color [trees.length];
  // treeCol[0]=color (255,0,0);
  //treeCol[1]=color(0,255,0);
  // treeCol[2]=color(255);
  for (int i=0; i<trees.length; i++)
  {
    int a=int(random(subDx));
    int b=int(random(subDy));
    int c=int(random(subDz));


    treeCol[i]=color(int(random(255)), int(random(255)), int(random(255)));

    maxDepth=5;
    // maxDepth=6-i;
    // branchCount=int(random(2, 4));
    // branchCount=4-i;
    int []thisSeed= {
      a, b, c
    };
    trees[i]= new Branch(0, thisSeed, treeCol[i]);
  }
  edges=new Edge[startIndVec.size()];
  for (int i=0; i<startIndVec.size(); i++)
  {
    edges[i]=new Edge((Vec3D)startIndVec.get(i), (Vec3D)endIndVec.get(i));
  }

  for (int i=0; i<edges.length; i++)
  {
    for (int j=0; j<edges.length; j++)
    {
      edges[i].getAligned(edges[j]);
    }
  }

  for (int i=0; i<edges.length; i++)
  {
    edges[i].getExtensions();
    println(edges[i].eAligned);
  }
  ///////////////////////////////LSys END
}


void draw()
{
  //println(cam.getDistance());
  handleView();
  background(255);
  // blendMode(BLEND);
  //lights();
  if (showUCS)
  {
    strokeWeight(1);
    stroke(255, 0, 0);
    line(0, 0, 0, 4000, 0, 0);
    stroke(0, 255, 0);
    line(0, 0, 0, 0, 4000, 0);
    stroke(0, 0, 255);
    line(0, 0, 0, 0, 0, 4000);
  }

  //strokeWeight(0.5);
  //stroke(128);
  //fill(255, 255, 0);
  if (showNodes)
  {
    for (int i = 0; i < subDx; i++)
    {
      for (int j = 0; j < subDy; j++)
      {
        for (int k = 0; k < subDz; k++)
        {
          if (cells[i][j][k].occupied) cells[i][j][k].draw();
          //cells[i][j][k].draw();
        }
      }
    }
  }
  ///////////////////////////////LSys draw
  for (int i=0; i<trees.length; i++)
  {
    // stroke(treeCol[i]);
    //  trees[i].draw();
  }

  for (int i=0; i<startIndVec.size(); i++)
  {

    edges[i].drawEdge();
  }
  ///////////////////////////////LSys draw END
  // blendMode(ADD);
  strokeWeight(0.5);
  //stroke(255, 255, 0, 92);
  stroke(150, 92);
  noFill();
  renderGridLines();
  stroke(150, 128);
  strokeWeight(3);
  /*  for (int i = 0; i < floorLvl.length; i++)
   {
   pushMatrix();
   translate((float)(centre.getX()), (float)(centre.getY()), floorLvl[i] - refLvl);
   rect(0, 0, slabSz, slabSz);
   translate(0, 0, -slabThickness);
   rect(0, 0, slabSz, slabSz);
   seedNumMatrix();
   }
   strokeWeight(1);
   rotateX(-PI/2);
   stroke(64, 128);
   model.draw();
   */
  cam.beginHUD();
  ortho();
  fill(0);
  textFont(font, 24);
  text("Camera Position in World Space:", 10, 10);
  text("X: " + nf(cam.getPosition()[0], 4, 2) + " / Y: " + nf(cam.getPosition()[1], 4, 2) + " / Z: " + nf(cam.getPosition()[2], 4, 2), 10, 40);
  text("This is a line!", 10, 70);
  text("This is another line!", 10, 100);
  text("Is there enough gap between the lines?", 10, 130);
  
  cam.endHUD();
 // G4P.draw();
}


void handleView()
{

  if (view!=0)
  {
    if (view==1)
    {
      cam.setState(topView);
    }
    if (view==2)
    {
      cam.setState(frontView);
    }
    else if (view==3)
    {
      cam.setState(leftView);
    }
    view = 0;
  }
  if (orthoMode) 
  {
    float leftClip = - (float(width)/float(height)) * ((float)cam.getDistance()) + float(height) * 0.5f;
    float rightClip = (float(width)/float(height)) * ((float)cam.getDistance()) + float(height) * 0.5f;
    float bottomClip = - (float)cam.getDistance() + float(height) * 0.5f;
    float topClip = (float)cam.getDistance() + float(height) * 0.5f;
    ortho(leftClip, rightClip, bottomClip, topClip, -20000, 20000);
  }
  else
  {
    perspective(PI/2f, float(width)/float(height), ((float(height)/2.0) / tan(PI/6f)) / 10000f, ((float(height)/2.0) / tan(PI/6f)) * 100f);
  }
}

void renderGridLines()
{
  for (int i = 0; i < subDx+1; i++)
  {
    for (int j = 0; j < subDy+1; j++)
    {
      line((i-0.5f)*cellSzX, (j-0.5f)*cellSzY, -0.5f*cellSzZ, (i-0.5f)*cellSzX, (j-0.5f)*cellSzY, (subDz-0.5f)*cellSzZ);
    }
  }
  for (int i = 0; i < subDx+1; i++)
  {
    for (int j = 0; j < subDz+1; j++)
    {
      line((i-0.5f)*cellSzX, -0.5f*cellSzY, (j-0.5f)*cellSzZ, (i-0.5f)*cellSzX, (subDy-0.5f)*cellSzY, (j-0.5f)*cellSzZ);
    }
  }
  for (int i = 0; i < subDy+1; i++)
  {
    for (int j = 0; j < subDz+1; j++)
    {
      line(-0.5f*cellSzX, (i-0.5f)*cellSzY, (j-0.5f)*cellSzZ, (subDx-0.5f)*cellSzX, (i-0.5f)*cellSzY, (j-0.5f)*cellSzZ);
    }
  }
}



void keyReleased() 
{
  if (key == '0') 
  { 
    view = 0;
  }
  if (key == '1') 
  {
    view = 1;
  }
  if (key == '2') 
  {
    view = 2;
  }
  if (key == '3') 
  {
    view = 3;
  }
  if (key == 'o' || key == 'O') orthoMode = true;
  if (key == 'p' || key == 'P') orthoMode = false;


  if (key == 'w' || key == 'W') white = true;
  if (key == 'c' || key == 'C') white = false;

  if (key == 'n' || key == 'N') 
  {

    counter++;
    if (counter%2==0)showNodes = true;
    else showNodes = false;
  }
  if (key == 'w' || key == 'W') 
  {
    counterB++;
    if (counterB%2==0)showUCS= true;
    else showUCS=  false;
  }
}
