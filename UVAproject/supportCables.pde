
ArrayList lowestOccupiedInds=new ArrayList(); //index
void supportCables()
{
  int step=1;
  for (int i = 0; i < subDx; i+=step)
  {
    for (int j = 0; j < subDy; j+=step)
    {
      for (int k = 0; k < subDz; k++)
      {
        if (cells[i][j][k].occupied)
        { 
          Index ocInd=cells[i][j][k].ind;
          lowestOccupiedInds.add(ocInd);
          break;
        }
      }
    }
  }
  println(lowestOccupiedInds.size());
}

void longSupportCables()
{
  int step=1;
  
    for (int i = 0; i < subDx; i+=step)
    {
      for (int j = 0; j < subDy; j+=step)
      {
        for (int m = 0; m < visibleEdges.size(); m++)
  {
    Edge e= (Edge) visibleEdges.get(m);
        if ((e.startInd.d[0]==i && e.startInd.d[1]==j)|| (e.endInd.d[0]==i && e.endInd.d[1]==j))
        {
          Index ocInd=cells[i][j][0].ind;
          lowestOccupiedInds.add(ocInd);
          break;
        }
      }
    }
  }
  println(lowestOccupiedInds.size());
}
void drawCables()
{
  for (int i=0; i< lowestOccupiedInds.size(); i++)
  {
    Index ind=(Index)lowestOccupiedInds.get(i);
    // line(ind.d[0]*cellSzX, ind.d[1]*cellSzY, ind.d[2]*cellSzZ, ind.d[0]*cellSzX, ind.d[1]*cellSzY, subDz*cellSzZ);//until lowest strut
    line(ind.d[0]*cellSzX, ind.d[1]*cellSzY, 0, ind.d[0]*cellSzX, ind.d[1]*cellSzY, subDz*cellSzZ); //until down
  }
}
