ArrayList<String> textFile = new ArrayList<String>();

//String[] textFile = new String[1000];

void createTextFile()
{
  String str;

  textFile.add("cellSzX: " + nf(cellSzX, 5, 3));
  textFile.add("cellSzY: " + nf(cellSzY, 5, 3));
  textFile.add("cellSzZ: " + nf(cellSzZ, 5, 3));

  textFile.add("subDx: " + nf(subDx, 4));
  textFile.add("subDy: " + nf(subDy, 4));
  textFile.add("subDz: " + nf(subDz, 4));

  textFile.add("TotalGenerations: " + nf(generations, 4));

  str = "generationSeeds: ";

  for (int i = 0; i < generations; i++)
  {
    str += generationSeeds[i][0] + "/" + generationSeeds[i][2] + " ";
  }
  textFile.add(str);

  str = "generationStepScale: ";
  for (int i = 0; i < generations; i++)
  {
    str += generationStepScale[i][0] + "/" + generationStepScale[i][2] + " ";
  }
  textFile.add(str);

  str = "generationDepthLimits: ";
  for (int i = 0; i < generations; i++)
  {
    str += generationDepthLimits[i][0] + "/" + generationDepthLimits[i][2] + " ";
  }
  textFile.add(str);

  //  str = "Seed(";
  //  for (int i = 0; i < seeds.size(); i++)
  //  {
  //    Seed s = (Seed)seeds.get(i);
  //    str += i + "): ";
  //    str += s.hierarchyLevel + "/";
  //    str += "(" + nf(s.ind.d[0], 3) + "," + nf(s.ind.d[1], 3) + "," + nf(s.ind.d[2], 3) + "," + "/";
  //    str += s.hasGoal + "/";
  //    if (s.hasGoal) str += 
  //  }

// for (int i = 0; i < subDx; i++)
//    {
//      for (int j = 0; j < subDy; j++)
//      {
//        for (int k = 0; k < subDz; k++)
//        {
//          str = "Cell(" + nf(i,3) + "," + nf(j,3) + "," + nf(k,3) + "):";
//          if (cell[i][j][k].occupied) str += "0/";
//          else str += "1";
//          cells[i][j][k];
//        }
//      }
//    }
//  str +=
}

// CELLS
//  Vec3D centre;
//  boolean occupied;
//  //int[] indices = new int[3];
//  ArrayList neighbours;
//  Index left, right, front, back, upper, lower;
//  int neighbourCount;
//  Index ind;
//  float brVal;
//  int hierarchyLevel;


//  int hierarchyLevel;  
//  int childBranchCount;
//  ArrayList childBranches;
//  Branch parentBranch;
//  Index ind;
//  boolean hasGoal;
//  Index goal;
//  boolean isRoot;
//  boolean active;
//  int stepScaleMin;
//  int stepScaleMax;
//  int depthLimitMin;
//  int depthLimitMax;
