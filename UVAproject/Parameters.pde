float cellSzX = 100 * 0.3;
float cellSzY = 100 * 0.15;
float cellSzZ = 100 * 0.45;

//int subDx = 12;
//int subDy = 6;
//int subDz = 15;

int subDx = int(390/cellSzX);
int subDy = int(155/cellSzY);
int subDz = int(2915/cellSzZ);

// These units are in meters. They need to be converted to cm (currently done in setup())
float[] floorLvl = {19.96, 23.46, 26.96, 32.14, 36.70, 40.98, 44.94, 49.06, 52.44};
float refLvl = 100 * (2.75 + 19.96); //cm
float slabThickness = 50; //cm
float slabSz = 1000f; //cm

float strutTh=2;

Cell[][][] cells = new Cell[subDx][subDy][subDz];

PeasyCam cam;
Vector3D centre = new Vector3D(0.5f * cellSzX * (subDx - 1), 0.5f * cellSzY * (subDy - 1), 0.5f * cellSzZ * (subDz - 1));
CameraState topView = new CameraState(new Rotation(new Vector3D(0, 0, 1), 0), centre, 2000);
//CameraState frontView = new CameraState(new Rotation(new Vector3D(1, 0, 0), -PI/2), centre, 2000);
CameraState frontView = new CameraState(new Rotation(RotationOrder.ZXY, PI, -PI/2, 0), centre, 2000);
CameraState leftView = new CameraState(new Rotation(RotationOrder.ZXY, -PI/2, -PI/2, 0), centre, 2000);



int generations = 5;
//int[][] generationSeeds = {{120,150}, {8,12}, {8,12}, {0,0},{3,15}}; 
//int[][] generationStepScale = {{1, 3}, {1, 2}, {1, 2}, {1, 1}, {1, 1}};
//int[][] generationDepthLimits = {{-1, -1}, {2,6}, {2,2}, {2,2}, {1,1}};

// For random seeding (not image-based)
//int[][] generationSeeds = {{15,25}, {12,20}, {3,8}, {0,0},{3,15}}; 
//int[][] generationStepScale = {{1, 3}, {3, 5}, {1, 1}, {1, 1}, {1, 1}};
//int[][] generationDepthLimits = {{-1, -1}, {2,6}, {2,2}, {2,2}, {1,1}};

int[][] generationSeeds = {{50,70}, {12,20}, {12,20}, {0,0},{3,15}}; 
int[][] generationStepScale = {{1, 3}, {1, 2}, {1, 2}, {1, 1}, {1, 1}};
int[][] generationDepthLimits = {{-1, -1}, {2,6}, {2,2}, {1,1}, {1,1}};

float [] distrPrcnt= {40, 30, 20, 10, 0};
//float [] distrPrcnt= {50,35, 10, 5, 0};
int view = 0;


boolean recordDXF=false;

//int [] subDzPerFloor=new int[floorLvl.length];

OBJModel model;
////////////////////////////LSys
//int seedNum=6; //20; //8; //set to 0 to get only spine, 4 for spines branching
//Branch [] trees=new Branch[seedNum];
//Cell [] seedPts=new Cell[seedNum];
//color [] treeCol=new color [seedNum];
//int maxDepth;
//int depthCeil=4;
int branchCount;
int nodeCount;
float totalEdgeLength=0;
float totalExtEdgeLength=0;
float totalVisibleEdgeLength=0;

ArrayList branches = new ArrayList();
ArrayList seeds = new ArrayList();

//Vec3D attractor;

//Edge [] edges;
//Spine s;

//boolean white;
int counter=0;
int counterB=0;
int counterC=0;
int counterD=0;
int counterE=0;

PFont font;

//String depths=" ";
//int indX=0;

Direction[] directions = new Direction[6];

Index[] anchors = new Index[5];
int[] rEdges;

PImage img;
PImage spineImg;
