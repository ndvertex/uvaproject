class Seed //extends Cell
{
  int hierarchyLevel;  
  int childBranchCount;
  ArrayList childBranches;
  Branch parentBranch;
  Index ind;
  boolean hasGoal;
  Index goal;
  boolean isRoot;
  boolean active;
  int stepScaleMin;
  int stepScaleMax;
  int depthLimitMin;
  int depthLimitMax;
  //  Branch (Seed _parentSeed, int _hierarchyLevel, int _stepScaleMin, int _stepScaleMax, int _depthLimit)
  Seed ()
  {
  }
  Seed (int _hierarchyLevel, Branch _parentBranch, Index _ind, int _stepScaleMin, int _stepScaleMax, Index _goal)
  {
    hierarchyLevel = _hierarchyLevel;
    parentBranch = _parentBranch;
    ind = _ind;
    stepScaleMin = _stepScaleMin;
    stepScaleMax = _stepScaleMax;
    goal = _goal;
    childBranches = new ArrayList();
    active = true;
    isRoot = false;
    hasGoal = true;
  }
  Seed (int _hierarchyLevel, Index _ind, int _stepScaleMin, int _stepScaleMax, Index _goal)
  {
    hierarchyLevel = _hierarchyLevel;
    ind = _ind;
    stepScaleMin = _stepScaleMin;
    stepScaleMax = _stepScaleMax;
    goal = _goal;
    childBranches = new ArrayList();
    active = true;
    isRoot = true;
    hasGoal = true;
  }
  Seed (int _hierarchyLevel, Branch _parentBranch, Index _ind, int _stepScaleMin, int _stepScaleMax, int _depthLimitMin, int _depthLimitMax)
  {
    hierarchyLevel = _hierarchyLevel;
    parentBranch = _parentBranch;
    ind = _ind;
    stepScaleMin = _stepScaleMin;
    stepScaleMax = _stepScaleMax;
    depthLimitMin = _depthLimitMin;
    depthLimitMax = _depthLimitMax;
    childBranches = new ArrayList();
    active = true;
    isRoot = false;
    hasGoal = false;
  }
  Seed (int _hierarchyLevel, Index _ind, int _stepScaleMin, int _stepScaleMax, int _depthLimitMin, int _depthLimitMax)
  {
    hierarchyLevel = _hierarchyLevel;
    ind = _ind;
    stepScaleMin = _stepScaleMin;
    stepScaleMax = _stepScaleMax;
    depthLimitMin = _depthLimitMin;
    depthLimitMax = _depthLimitMax;
    childBranches = new ArrayList();
    active = true;
    isRoot = true;
    hasGoal = false;
  }
  //  void growBranchFromSeed()
  //  {
  //    Branch b = new Branch(this);
  //    if (! (b.reachedGoal ()==true || b.reachedDepthLimit()==true || b.deadEnd==true))
  //    {
  //      b.growOneStep();
  //    }
  //    branches.add(b);
  //    this.childBranches.add(b);
  //    this.active = false;
  //  }

  void startGeneratingBranch()
  {
    Branch b = new Branch(this);
    this.childBranches.add(b);
  }
  void endGeneratingBranch(Branch b)
  {
    branches.add(b);
    //this.active = false;
  }
}
