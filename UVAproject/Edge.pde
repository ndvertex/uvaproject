class Edge
{
  Index startInd;
  Index endInd;
  //  int [] startInd= new int[3];
  //int [] endInd= new int[3];
  float sX, sY, sZ, eX, eY, eZ;
  float extPor=0.25;
  Vec3D thisEdgeVec;
  Vec3D startVec; 
  Vec3D endVec;
  boolean sAligned=false;
  boolean eAligned=false;
  float edgeLength;

  int hierarchyLevel;
  Direction edgeDir;
  Direction LEDfaceDir;
  int visible;

  Edge (Index _startInd, Index _endInd)
  {
    startInd = _startInd;
    endInd = _endInd;
    startVec = new Vec3D(startInd.d[0], startInd.d[1], startInd.d[2]); 
    endVec = new Vec3D(endInd.d[0], endInd.d[1], endInd.d[2]);
    // startVec=_startVec;
    // endVec=_endVec;
    thisEdgeVec=endVec.copy();
    thisEdgeVec.subSelf(startVec);

    // println("xcomp:"+thisEdgeVec.x);
    // println("ycomp:"+thisEdgeVec.y);
    //println("zcomp:"+thisEdgeVec.z);

    if (thisEdgeVec.x==1) 
    {
      edgeDir=directions[0];
    }
    else if (thisEdgeVec.x==-1) 
    { 
      edgeDir=directions[1];
    }
    else if (thisEdgeVec.y==1)
    {  
      edgeDir=directions[2];
    }
    else if (thisEdgeVec.y==-1) 
    { 
      edgeDir=directions[3];
    }
    else if (thisEdgeVec.z==1)
    {
      edgeDir=directions[4];
    }
    else if (thisEdgeVec.z==-1) 
    {
      edgeDir=directions[5];
    }

    ////////////
    sX= cells[startInd.d[0]][startInd.d[1]][startInd.d[2]].centre.x;
    sY= cells[startInd.d[0]][startInd.d[1]][startInd.d[2]].centre.y;
    sZ= cells[startInd.d[0]][startInd.d[1]][startInd.d[2]].centre.z;

    eX= cells[endInd.d[0]][endInd.d[1]][endInd.d[2]].centre.x;
    eY= cells[endInd.d[0]][endInd.d[1]][endInd.d[2]].centre.y;
    eZ= cells[endInd.d[0]][endInd.d[1]][endInd.d[2]].centre.z;
    /////////////////////
  }

  void getAligned(Edge other)
  {
    this.eAligned=false;
    this.sAligned=false;
    float ang=other.thisEdgeVec.angleBetween(this.thisEdgeVec);
    if (ang==0 || ang==PI)
    {
      if (this.startVec.distanceTo(other.startVec)==0 || this.startVec.distanceTo(other.endVec)==0) 
      {
        this.sAligned=true;
        // println("saligned");
      }
      else this.sAligned=false;
      if (this.endVec.distanceTo(other.startVec)==0 || this.endVec.distanceTo(other.endVec)==0 )
      {
        this.eAligned=true;
        //println("ealigned");
      }
      else this.eAligned=false;
    }
  }

  void getExtensions()
  {
    if (this.sAligned==false)
    {
      if (edgeDir==directions[0]) sX-= extPor* cellSzX;
      if (edgeDir==directions[1]) sX+= extPor* cellSzX;
      if (edgeDir==directions[2]) sY-= extPor* cellSzY;
      if (edgeDir==directions[3]) sY+= extPor* cellSzY;
      if (edgeDir==directions[4]) sZ-= extPor* cellSzZ;
      if (edgeDir==directions[5]) sZ+= extPor* cellSzZ;
    }
    if (this.eAligned==false)
    {
      if (edgeDir==directions[0]) eX+= extPor* cellSzX;
      if (edgeDir==directions[1]) eX-= extPor* cellSzX;
      if (edgeDir==directions[2]) eY+= extPor* cellSzY;
      if (edgeDir==directions[3]) eY-= extPor* cellSzY;
      if (edgeDir==directions[4]) eZ+= extPor* cellSzZ;
      if (edgeDir==directions[5]) eZ-= extPor* cellSzZ;
    }
  }

  float getEdgeLength()
  { 
    edgeLength=dist(sX, sY, sZ, eX, eY, eZ);
    return edgeLength;
  }

  void drawEdge()
  {
    line(sX, sY, sZ, eX, eY, eZ);
  }
}
