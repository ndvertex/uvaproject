class Cell implements Comparable
{
  Vec3D centre;
  boolean occupied;
  //int[] indices = new int[3];
  ArrayList neighbours;
  Index left, right, front, back, upper, lower;
  int neighbourCount;
  Index ind;
  float brVal;
  int hierarchyLevel;

  Cell() 
  {
  }

  Cell(int _i, int _j, int _k, float _x, float _y, float _z)
  {
    centre = new Vec3D(_x, _y, _z);
    ind = new Index(_i, _j, _k);
    occupied = false;
    neighbours = new ArrayList();
  }

  void draw(color c, int strW)
  {
    pushMatrix();
    translate(centre.x, centre.y, centre.z);
    pushStyle();
    noStroke();
    // stroke(1)
    fill(0,255-brVal);
    box(cellSzX, cellSzY, cellSzZ);
    strokeWeight(strW);
    stroke(c);
    point(0, 0, 0);
    popStyle();
    // box(cellSz);
    popMatrix();
  }

  void getNeighbours()
  {
    int i = ind.d[0];
    int j = ind.d[1];
    int k = ind.d[2];

    if (i-1>=0) 
    {
      left = new Index(ind.d[0]-1,ind.d[1],ind.d[2]);
      //left = cells[i-1][j][k].ind;
      neighbours.add(left);
    }
    if (i+1<subDx) 
    {
      right = new Index(ind.d[0]+1,ind.d[1],ind.d[2]);
      //right = cells[i+1][j][k].ind;
      neighbours.add(right);
    }

    if (j-1>=0) 
    {
      back = new Index(ind.d[0],ind.d[1]-1,ind.d[2]);
      //back = cells[i][j-1][k].ind;
      neighbours.add(back);
    }
    if (j+1<subDy) 
    {
      front = new Index(ind.d[0],ind.d[1]+1,ind.d[2]);
      //front = cells[i][j+1][k].ind;
      neighbours.add(front);
    }

    if (k-1>=0) 
    {
      lower = new Index(ind.d[0],ind.d[1],ind.d[2]-1);
      //lower = cells[i][j][k-1].ind;
      neighbours.add(lower);
    }
    if (k+1<subDz) 
    {
      upper = new Index(ind.d[0],ind.d[1],ind.d[2]+1);
      //upper = cells[i][j][k+1].ind;
      neighbours.add(upper);
    }
    neighbourCount = neighbours.size();
  }

  ArrayList getInactiveNeighbours()
  {
    ArrayList inactiveNeighbours = new ArrayList();
    for (int i = 0; i < neighbours.size(); i++)
    {
      Index c = (Index)neighbours.get(i);
      if (cells[c.d[0]][c.d[1]][c.d[2]].occupied==false) inactiveNeighbours.add(c);
    }
    return inactiveNeighbours;
  }

  int compareTo(Object _other)
  {
    Cell other=(Cell) _other;
    if (other.brVal> brVal) return -1;
    if (other.brVal== brVal) return 0;
    else return 1;
  }
}
