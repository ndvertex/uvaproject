boolean dispNodes = false;
boolean dispGrid = false;
boolean dispSlabs = false;
boolean dispModel = false;
boolean dispUCS = false;
boolean dispBlackBranches = true;
boolean dispExtensions = true;
boolean dispBoxes = false;
boolean dispSuspensionCables = false;
boolean dispImage = false;

boolean orthoMode = false;
boolean generateNewTree = true;
boolean affectSelectedGen = false;

int st = 16;
Font guiFont = new Font("Tahoma", Font.PLAIN, 12);
Font guiFontSmall = new Font("Tahoma", Font.PLAIN, 12);
GWindow GUIwindow;

GLabel LBL_guiTitle;

GCheckbox CHK_dispNodes;
GCheckbox CHK_dispGrid;
GCheckbox CHK_dispSlabs;
GCheckbox CHK_dispModel; 
GCheckbox CHK_dispUCS; 
GCheckbox CHK_dispBlackBranches;
GCheckbox CHK_dispExtensions;
GCheckbox CHK_dispBoxes;
GCheckbox CHK_dispSuspensionCables;
GCheckbox CHK_dispImage;

GCheckbox CHK_ortho; 

GLabel LBL_viewMode;
GButton BTN_topView;
GButton BTN_frontView;
GButton BTN_leftView;

GLabel LBL_genViewed;
GSlider SDR_genViewed;
GLabel LBL_genSeedsMin;
GTextField TXT_genSeedsMin;
GLabel LBL_genSeedsMax;
GTextField TXT_genSeedsMax;
GLabel LBL_genStepScaleMin;
GTextField TXT_genStepScaleMin;
GLabel LBL_genStepScaleMax;
GTextField TXT_genStepScaleMax;
GLabel LBL_genDepthLimitMin;
GTextField TXT_genDepthLimitMin;
GLabel LBL_genDepthLimitMax;
GTextField TXT_genDepthLimitMax;

GCheckbox CHK_affectSelectedGen;
GButton BTN_update;

public void createGUI() 
{
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(8);
  G4P.setCursor(ARROW);
  if (frame != null)  frame.setTitle("Sketch Window");
  GUIwindow = new GWindow(this, "GUI Controls", 0, 100, 16 * st, 63 * st, false, JAVA2D);
  GUIwindow.setActionOnClose(G4P.EXIT_APP);
  GUIwindow.setBackground(192);
  //GUIwindow.addDrawHandler(this, "win_draw1");

  LBL_guiTitle = new GLabel(GUIwindow.papplet, 1 * st, 1 * st, 14 * st, 1 * st);
  LBL_guiTitle.setFont(guiFont);
  LBL_guiTitle.setText("CONTROLS");
  LBL_guiTitle.setTextBold();

  CHK_dispNodes = new GCheckbox(GUIwindow.papplet, 1 * st, 3 * st, 14 * st, 1 * st);
  CHK_dispNodes.setFont(guiFont);
  CHK_dispNodes.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispNodes.setText("Display Nodes");
  CHK_dispNodes.setTextBold();
  CHK_dispNodes.setSelected(true);

  CHK_dispGrid = new GCheckbox(GUIwindow.papplet, 1 * st, 5 * st, 14 * st, 1 * st);
  CHK_dispGrid.setFont(guiFont);
  CHK_dispGrid.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispGrid.setText("Display Grid");
  CHK_dispGrid.setTextBold();
  CHK_dispGrid.setSelected(false);

  CHK_dispSlabs = new GCheckbox(GUIwindow.papplet, 1 * st, 7 * st, 14 * st, 1 * st);
  CHK_dispSlabs.setFont(guiFont);
  CHK_dispSlabs.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispSlabs.setText("Display Slabs");
  CHK_dispSlabs.setTextBold();
  CHK_dispSlabs.setSelected(false);

  CHK_dispModel = new GCheckbox(GUIwindow.papplet, 1 * st, 9 * st, 14 * st, 1 * st);
  CHK_dispModel.setFont(guiFont);
  CHK_dispModel.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispModel.setText("Display 3D model");
  CHK_dispModel.setTextBold();
  CHK_dispModel.setSelected(false);

  CHK_dispUCS = new GCheckbox(GUIwindow.papplet, 1 * st, 11 * st, 14 * st, 1 * st);
  CHK_dispUCS.setFont(guiFont);
  CHK_dispUCS.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispUCS.setText("Display UCS");
  CHK_dispUCS.setTextBold();
  CHK_dispUCS.setSelected(false);

  CHK_dispBlackBranches = new GCheckbox(GUIwindow.papplet, 1 * st, 13 * st, 14 * st, 1 * st);
  CHK_dispBlackBranches.setFont(guiFont);
  CHK_dispBlackBranches.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispBlackBranches.setText("Display black branches");
  CHK_dispBlackBranches.setTextBold();
  CHK_dispBlackBranches.setSelected(true);

  CHK_dispExtensions = new GCheckbox(GUIwindow.papplet, 1 * st, 15 * st, 14 * st, 1 * st);
  CHK_dispExtensions.setFont(guiFont);
  CHK_dispExtensions.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispExtensions.setText("Display extensions");
  CHK_dispExtensions.setTextBold();
  CHK_dispExtensions.setSelected(true);

  CHK_dispBoxes = new GCheckbox(GUIwindow.papplet, 1 * st, 17 * st, 14 * st, 1 * st);
  CHK_dispBoxes.setFont(guiFont);
  CHK_dispBoxes.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispBoxes.setText("Display Solid Struts");
  CHK_dispBoxes.setTextBold();
  CHK_dispBoxes.setSelected(false);

  CHK_dispSuspensionCables = new GCheckbox(GUIwindow.papplet, 1 * st, 19 * st, 14 * st, 1 * st);
  CHK_dispSuspensionCables.setFont(guiFont);
  CHK_dispSuspensionCables.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispSuspensionCables.setText("Display Suspension Cables");
  CHK_dispSuspensionCables.setTextBold();
  CHK_dispSuspensionCables.setSelected(false);

  CHK_dispImage = new GCheckbox(GUIwindow.papplet, 1 * st, 21 * st, 14 * st, 1 * st);
  CHK_dispImage.setFont(guiFont);
  CHK_dispImage.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_dispImage.setText("Display Image");
  CHK_dispImage.setTextBold();
  CHK_dispImage.setSelected(false);

  CHK_ortho = new GCheckbox(GUIwindow.papplet, 1 * st, 23 * st, 14 * st, 1 * st);
  CHK_ortho.setFont(guiFont);
  CHK_ortho.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_ortho.setText("Orthographic Projection");
  CHK_ortho.setTextBold();
  CHK_ortho.setOpaque(false);
  CHK_ortho.setSelected(false);


  LBL_viewMode = new GLabel(GUIwindow.papplet, 1 * st, 25 * st, 14 * st, 1 * st);
  LBL_viewMode.setTextAlign(GAlign.LEFT, null);
  LBL_viewMode.setFont(guiFont);
  LBL_viewMode.setText("View Mode:");
  LBL_viewMode.setTextBold();

  BTN_topView = new GButton(GUIwindow.papplet, 1 * st, 27 * st, 4 * st, 1 * st);
  BTN_topView.setFont(guiFontSmall);
  BTN_topView.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  BTN_topView.setText("TOP");

  BTN_frontView = new GButton(GUIwindow.papplet, 6 * st, 27 * st, 4 * st, 1 * st);
  BTN_frontView.setFont(guiFontSmall);
  BTN_frontView.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  BTN_frontView.setText("FRONT");

  BTN_leftView = new GButton(GUIwindow.papplet, 11 * st, 27 * st, 4 * st, 1 * st);
  BTN_leftView.setFont(guiFontSmall);
  BTN_leftView.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  BTN_leftView.setText("LEFT");

  LBL_genViewed = new GLabel(GUIwindow.papplet, 1 * st, 29.5 * st, 14 * st, 1 * st);
  LBL_genViewed.setTextAlign(GAlign.LEFT, null);
  LBL_genViewed.setFont(guiFont);
  LBL_genViewed.setText("Select generation to edit:");
  LBL_genViewed.setTextBold();

  SDR_genViewed = new GSlider(GUIwindow.papplet, 1 * st, 30.5 * st, 14 * st, 4 * st, 1 * st);
  SDR_genViewed.setLimits(2, 0, generations-1);
  SDR_genViewed.setNbrTicks(generations);
  SDR_genViewed.setShowValue(true);
  SDR_genViewed.setShowLimits(true);
  SDR_genViewed.setStickToTicks(true);
  SDR_genViewed.setNumberFormat(G4P.INTEGER, 0);

  LBL_genSeedsMin = new GLabel(GUIwindow.papplet, 1 * st, 35 * st, 5.5 * st, 1 * st);
  LBL_genSeedsMin.setTextAlign(GAlign.RIGHT, null);
  LBL_genSeedsMin.setFont(guiFont);
  LBL_genSeedsMin.setText("MinSeeds:");
  TXT_genSeedsMin = new GTextField(GUIwindow.papplet, 6.5 * st, 35 * st, 1.5 * st, 1 * st);
  TXT_genSeedsMin.setText(str(generationSeeds[SDR_genViewed.getValueI()][0]));

  LBL_genSeedsMax = new GLabel(GUIwindow.papplet, 8 * st, 35 * st, 5.5 * st, 1 * st);
  LBL_genSeedsMax.setTextAlign(GAlign.RIGHT, null);
  LBL_genSeedsMax.setFont(guiFont);
  LBL_genSeedsMax.setText("MaxSeeds:");
  TXT_genSeedsMax = new GTextField(GUIwindow.papplet, 13.5 * st, 35 * st, 1.5 * st, 1 * st);
  TXT_genSeedsMax.setText(str(generationSeeds[SDR_genViewed.getValueI()][1]));

  LBL_genStepScaleMin = new GLabel(GUIwindow.papplet, 1 * st, 37 * st, 5.5 * st, 1 * st);
  LBL_genStepScaleMin.setTextAlign(GAlign.RIGHT, null);
  LBL_genStepScaleMin.setFont(guiFont);
  LBL_genStepScaleMin.setText("MinStepScale:");
  TXT_genStepScaleMin = new GTextField(GUIwindow.papplet, 6.5 * st, 37 * st, 1.5 * st, 1 * st);
  TXT_genStepScaleMin.setText(str(generationStepScale[SDR_genViewed.getValueI()][0]));

  LBL_genStepScaleMax = new GLabel(GUIwindow.papplet, 8 * st, 37 * st, 5.5 * st, 1 * st);
  LBL_genStepScaleMax.setTextAlign(GAlign.RIGHT, null);
  LBL_genStepScaleMax.setFont(guiFont);
  LBL_genStepScaleMax.setText("MaxStepScale:");
  TXT_genStepScaleMax = new GTextField(GUIwindow.papplet, 13.5 * st, 37 * st, 1.5 * st, 1 * st);
  TXT_genStepScaleMax.setText(str(generationStepScale[SDR_genViewed.getValueI()][1]));

  LBL_genDepthLimitMin = new GLabel(GUIwindow.papplet, 1 * st, 39 * st, 5.5 * st, 1 * st);
  LBL_genDepthLimitMin.setTextAlign(GAlign.RIGHT, null);
  LBL_genDepthLimitMin.setFont(guiFont);
  LBL_genDepthLimitMin.setText("MinDepthLimit:");
  TXT_genDepthLimitMin = new GTextField(GUIwindow.papplet, 6.5 * st, 39 * st, 1.5 * st, 1 * st);
  TXT_genDepthLimitMin.setText(str(generationDepthLimits[SDR_genViewed.getValueI()][0]));

  LBL_genDepthLimitMax = new GLabel(GUIwindow.papplet, 8 * st, 39 * st, 5.5 * st, 1 * st);
  LBL_genDepthLimitMax.setTextAlign(GAlign.RIGHT, null);
  LBL_genDepthLimitMax.setFont(guiFont);
  LBL_genDepthLimitMax.setText("MaxDepthLimit:");
  TXT_genDepthLimitMax = new GTextField(GUIwindow.papplet, 13.5 * st, 39 * st, 1.5 * st, 1 * st);
  TXT_genDepthLimitMax.setText(str(generationDepthLimits[SDR_genViewed.getValueI()][1]));

  CHK_affectSelectedGen = new GCheckbox(GUIwindow.papplet, 1 * st, 41 * st, 14 * st, 2 * st);
  CHK_affectSelectedGen.setFont(guiFont);
  CHK_affectSelectedGen.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  CHK_affectSelectedGen.setText("Affect only selected and subsequent hierarchy level");
  CHK_affectSelectedGen.setTextBold();
  CHK_affectSelectedGen.setSelected(false);

  BTN_update = new GButton(GUIwindow.papplet, 1 * st, 44 * st, 14 * st, 2 * st);
  BTN_update.setFont(guiFont);
  BTN_update.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  BTN_update.setText("UPDATE TREE");
  BTN_update.setTextBold();
}

public void handleToggleControlEvents(GToggleControl chk, GEvent event) 
{
  if (chk == CHK_dispNodes) 
  {
    dispNodes = chk.isSelected();
  }
  if (chk == CHK_dispGrid) 
  {
    dispGrid = chk.isSelected();
  }
  if (chk == CHK_dispSlabs) 
  {
    dispSlabs = chk.isSelected();
  }
  if (chk == CHK_dispModel) 
  {
    dispModel = chk.isSelected();
  }
  if (chk == CHK_dispUCS) 
  {
    dispUCS = chk.isSelected();
  }
  if (chk == CHK_ortho) 
  {
    orthoMode = chk.isSelected();
  }
  if (chk == CHK_dispBlackBranches) 
  {
    dispBlackBranches = chk.isSelected();
  }
  if (chk == CHK_dispExtensions) 
  {
    dispExtensions = chk.isSelected();
  }
  if (chk == CHK_dispBoxes) 
  {
    dispBoxes = chk.isSelected();
  }
  if (chk == CHK_dispSuspensionCables) 
  {
    dispSuspensionCables = chk.isSelected();
  }
  if (chk == CHK_dispImage) 
  {
    dispImage = chk.isSelected();
  }


  if (chk == CHK_affectSelectedGen) 
  {
    affectSelectedGen = chk.isSelected();
  }
}

public void handleButtonEvents(GButton button, GEvent event) 
{
  if (button == BTN_topView && event == GEvent.CLICKED) 
  {
    view = 1;
  }
  if (button == BTN_frontView && event == GEvent.CLICKED) 
  {
    view = 2;
  }
  if (button == BTN_leftView && event == GEvent.CLICKED) 
  {
    view = 3;
  }
  if (button == BTN_update && event == GEvent.CLICKED) 
  {
    generateNewTree = true;
  }
}

public void handleTextEvents(GEditableTextControl textcontrol, GEvent event)
{
  if (textcontrol == TXT_genSeedsMin)
  {
    generationSeeds[SDR_genViewed.getValueI()][0] = int(TXT_genSeedsMin.getText());
  }
  if (textcontrol == TXT_genSeedsMax)
  {
    generationSeeds[SDR_genViewed.getValueI()][1] = int(TXT_genSeedsMax.getText());
  }
  if (textcontrol == TXT_genStepScaleMin)
  {
    generationStepScale[SDR_genViewed.getValueI()][0] = int(TXT_genStepScaleMin.getText());
  }
  if (textcontrol == TXT_genStepScaleMax)
  {
    generationStepScale[SDR_genViewed.getValueI()][1] = int(TXT_genStepScaleMax.getText());
  }
  if (textcontrol == TXT_genDepthLimitMin)
  {
    generationDepthLimits[SDR_genViewed.getValueI()][0] = int(TXT_genDepthLimitMin.getText());
  }
  if (textcontrol == TXT_genDepthLimitMax)
  {
    generationDepthLimits[SDR_genViewed.getValueI()][1] = int(TXT_genDepthLimitMax.getText());
  }
}


public void handleSliderEvents(GValueControl slider, GEvent event) 
{
  if (slider == SDR_genViewed) 
  {
    TXT_genSeedsMin.setText(str(generationSeeds[SDR_genViewed.getValueI()][0]));
    TXT_genSeedsMax.setText(str(generationSeeds[SDR_genViewed.getValueI()][1]));
    TXT_genStepScaleMin.setText(str(generationStepScale[SDR_genViewed.getValueI()][0]));
    TXT_genStepScaleMax.setText(str(generationStepScale[SDR_genViewed.getValueI()][1]));
    TXT_genDepthLimitMin.setText(str(generationDepthLimits[SDR_genViewed.getValueI()][0]));
    TXT_genDepthLimitMax.setText(str(generationDepthLimits[SDR_genViewed.getValueI()][1]));
  }
}
