class Branch //<>// //<>// //<>//
{
  Seed parentSeed;
  ArrayList childSeeds;
  int seedCount;
  int seedCountMin;
  int seedCountMax;
  int hierarchyLevel;  

  ArrayList branchCells;

  int stepScaleMin;
  int stepScaleMax;
  int trajectoryStepsRequired;
  int trajectoryStepsTaken;
  boolean completedTrajectory;

  boolean hasGoal;
  Index goal;

  boolean hasDepthLimit;
  int depthLimitMin;
  int depthLimitMax;
  int depthLimit;

  Index header;
  Direction currentDir;
  Direction prevDir;

  boolean deadEnd;

  color col;

  Branch (Seed _parentSeed)
  {
    parentSeed = _parentSeed;
    hierarchyLevel = parentSeed.hierarchyLevel;
    stepScaleMin = parentSeed.stepScaleMin;
    stepScaleMax = parentSeed.stepScaleMax;
    if (parentSeed.hasGoal)
    {
      goal = parentSeed.goal; 
      hasGoal = true;
      hasDepthLimit = false;
    }
    else
    {
      hasGoal = false;
      hasDepthLimit = true;
      depthLimitMin = parentSeed.depthLimitMin;
      depthLimitMax = parentSeed.depthLimitMax;
      depthLimit = round(random(depthLimitMin, depthLimitMax));
    }  
    header = _parentSeed.ind;
    Index ind = new Index(header.d[0], header.d[1], header.d[2]);
    cells[header.d[0]][header.d[1]][header.d[2]].occupied = true;
    cells[header.d[0]][header.d[1]][header.d[2]].hierarchyLevel = hierarchyLevel;
    branchCells = new ArrayList();
    branchCells.add(ind);
    currentDir = new Direction();
    if (parentSeed.isRoot) 
    {
      prevDir = new Direction(0, 0, 1);
    }
    else 
    {
      int dice = int(random(directions.length));
      prevDir =new Direction();
      for (int i = 0 ; i < prevDir.d.length ; i++)
      {
        prevDir.d[i] = directions[dice].d[i];
      }
    }
    deadEnd = false;
    completedTrajectory = false;
    trajectoryStepsRequired = round(random(stepScaleMin, stepScaleMax));
    trajectoryStepsTaken = 0;
    trajectoryStepsTaken++;
    col = color(int(random(255)), int(random(255)), int(random(255)));
  }  

  void draw()
  {
    if (dispBlackBranches) 
    {
      strokeWeight(1);
      stroke(0);
    }
    else
    {
      strokeWeight(4);
      if (hierarchyLevel == 0) stroke(255, 0, 0);
      else if (hierarchyLevel == 1) stroke(0, 255, 0);
      else if (hierarchyLevel == 2) stroke(0, 0, 255);
      else if (hierarchyLevel == 3) stroke(255, 0, 255);
      else if (hierarchyLevel == 4) stroke(0, 255, 255);
      else if (hierarchyLevel == 4) stroke(255, 255, 0);
      else stroke(0);
    }
    //    stroke(0, 255 - 64 * hierarchyLevel);
    for (int i = 0 ; i < branchCells.size()-1; i++)
    {
      Index startPt = (Index)branchCells.get(i);
      Index endPt = (Index)branchCells.get(i+1);
      //float alpha = map(i, 0, branchCells.size(), 0, 255);
      //stroke(red(col), green(col), blue(col), 255);
      line(startPt.d[0] * cellSzX, startPt.d[1] * cellSzY, startPt.d[2] * cellSzZ, endPt.d[0] * cellSzX, endPt.d[1] * cellSzY, endPt.d[2] * cellSzZ);
    }
  }
  void growOneStep()
  {
    Direction currentDir = new Direction();
    Index result = new Index(); 
    completedTrajectory = false;
    if (!completedTrajectory && trajectoryStepsTaken >= trajectoryStepsRequired)
    {
      completedTrajectory = true;
    }
    else if (!completedTrajectory && trajectoryStepsTaken < trajectoryStepsRequired)
    {   
      for (int i = 0; i < header.d.length; i++)
      {
        result.d[i] = header.d[i] + prevDir.d[i];
      }
      if (result.isWithinBounds())
      {
        if (cells[result.d[0]][result.d[1]][result.d[2]].occupied == false)
        {
          for (int i = 0; i < currentDir.d.length; i++)
          {
            currentDir.d[i] = prevDir.d[i];
          }
        }
        else completedTrajectory = true;
      }
      else completedTrajectory = true;
    }
    if (completedTrajectory)
    {
      currentDir = chooseNewDirection(header);
      if (currentDir.d[0]==0 && currentDir.d[1]==0 && currentDir.d[2]==0) deadEnd = true;
      else if (hierarchyLevel==0 && header.d[2] >= subDz-1) deadEnd = true; /// TAKE THIS OUT WHEN SPINE (LEVEL 0) HAS PROPER GOAL SEEKING FUNCTIONALITY
      else
      {
        trajectoryStepsRequired = round(random(stepScaleMin, stepScaleMax));
        trajectoryStepsTaken = 0;
      }
    }
    if (!deadEnd)
    {
      for (int i = 0; i < header.d.length; i++)
      {
        header.d[i] += currentDir.d[i];
      }
      cells[header.d[0]][header.d[1]][header.d[2]].occupied = true;
      cells[header.d[0]][header.d[1]][header.d[2]].hierarchyLevel = hierarchyLevel;
      Index ind = new Index(header.d[0], header.d[1], header.d[2]);
      branchCells.add(ind);
      trajectoryStepsTaken++;
      for (int i = 0; i < currentDir.d.length; i++)
      {
        prevDir.d[i] = currentDir.d[i];
      }
    }
    //println("Hierarchy@: " + hierarchyLevel);
  }
  Direction chooseNewDirection(Index currentInd)
  {
    Direction dir = new Direction();
    Cell c = cells[currentInd.d[0]][currentInd.d[1]][currentInd.d[2]];
    ArrayList emptyNeighbours = c.getInactiveNeighbours();
    if (emptyNeighbours.size()>0)
    {
      if (hierarchyLevel==0 && emptyNeighbours.contains(c.lower)) emptyNeighbours.remove(c.lower);
      if (hierarchyLevel==0 && emptyNeighbours.contains(c.upper))
      {
        float dice = random(100);
        if (dice>30)
        {
          dir = new Direction(0, 0, 1);
        }
        else
        {
          int dice2 = int(random(emptyNeighbours.size()));
          Index cNew = (Index)emptyNeighbours.get(dice2);
          for (int i = 0; i < dir.d.length; i++)
          {
            dir.d[i] = cNew.d[i] - currentInd.d[i];
          }
        }
      }
      //////////////
      
      else
      {
        int dice = int(random(emptyNeighbours.size()));
        Index cNew = (Index)emptyNeighbours.get(dice);
        for (int i = 0; i < dir.d.length; i++)
        {
          dir.d[i] = cNew.d[i] - currentInd.d[i];
        }
      }
    }
    else dir = new Direction(0, 0, 0);

    // Anna enter code here....
    //.....
    //......
    return dir;
  }
 
 
  void sowSeedsOnBranch(int _seedCountMin, int _seedCountMax, int _stepScaleMin, int _stepScaleMax, int _depthLimitMin, int _depthLimitMax)
  {
    seedCountMin = _seedCountMin; 
    seedCountMax = _seedCountMax;
    seedCount = round(random(seedCountMin, seedCountMax));
    childSeeds = new ArrayList();
    boolean generatedSeedsOnBranch = false;
    while (!generatedSeedsOnBranch)
    {
      int r = int(random(branchCells.size()));
      Index candidateCell = (Index)branchCells.get(r);
      Index randomCellOnBranch = new Index();
      for (int i = 0 ; i < 3; i++)
      {
        randomCellOnBranch.d[i] = candidateCell.d[i];
      }
      Seed s = new Seed(hierarchyLevel+1, this, randomCellOnBranch, _stepScaleMin, _stepScaleMax, _depthLimitMin, _depthLimitMax);
      if (!childSeeds.contains(s)) 
      {
        childSeeds.add(s);
        seeds.add(s);
      }
      if (childSeeds.size() >= seedCount) 
      {
        generatedSeedsOnBranch = true;
      }
    }
  }

  void sowSeedsOnBranchByBrightness(int _seedCountMin, int _seedCountMax, int _stepScaleMin, int _stepScaleMax, int _depthLimitMin, int _depthLimitMax)
  {
    //Cell [] distributeSeeds(Spine _s, int _seedNum)
    // {
    //   Cell [] spCells= new Cell [_s.spineVecs.size()];
    //     Cell [] selectedSeeds=new Cell [_seedNum];
    seedCountMin = _seedCountMin; 
    seedCountMax = _seedCountMax;
    seedCount = round(random(seedCountMin, seedCountMax));
    childSeeds = new ArrayList();
    childSeeds.clear();

    float incr = branchCells.size()/distrPrcnt.length;
    int [] newSeedIndexOnBranch= new int[seedCount];
    int counter=0;
    ArrayList orderedBranchCellsByBrightness = new ArrayList();

    for (int i=0; i< branchCells.size(); i++)
    {
      Index cellIndex = (Index)branchCells.get(i);
      Cell c = cells[cellIndex.d[0]][cellIndex.d[1]][cellIndex.d[2]];
      orderedBranchCellsByBrightness.add(c);
    }

    Collections.sort(orderedBranchCellsByBrightness);

    ArrayList orderedBranchIndicesByBrightness = new ArrayList();
    for (int i=0; i< orderedBranchCellsByBrightness.size(); i++)
    {
      Cell c = (Cell)orderedBranchCellsByBrightness.get(i);
      Index indTemp = new Index();
      for (int j = 0 ; j < c.ind.d.length; j++)
      {
        indTemp.d[j] = c.ind.d[j];
      }
      orderedBranchIndicesByBrightness.add(indTemp);
    }


    for (int i=0; i<distrPrcnt.length; i++)
    {
      int n = int(distrPrcnt[i] * seedCount / 100);
      for (int j=0; j<n; j++)
      {
        newSeedIndexOnBranch[counter] = int(random(incr*i, incr*(i+1)));
        counter++;
      }
    }

    //println("counter "+counter);
    for (int i=0; i<seedCount; i++)
    {
      int a = newSeedIndexOnBranch[i];
      Index candidateCell = (Index)orderedBranchIndicesByBrightness.get(a);
      Index randomCellOnBranch = new Index();
      for (int j = 0 ; j < 3; j++)
      {
        randomCellOnBranch.d[j] = candidateCell.d[j];
      }
      // Cell c = (Cell)branchCells.get(a);
      Seed s = new Seed(hierarchyLevel+1, this, randomCellOnBranch, _stepScaleMin, _stepScaleMax, _depthLimitMin, _depthLimitMax);
      childSeeds.add(s);
      seeds.add(s);
      //selectedSeeds[i]= spCells[newSeedIndexOnBranch[i]];
    }
    //   return(selectedSeeds);
    // }
  }


  boolean reachedGoal()
  {
    boolean rG = false;
    if (header==goal) rG = true;
    //else rG = false;
    return rG;
  }
  boolean reachedDepthLimit()
  {
    boolean rDL = false;
    if (hasDepthLimit && branchCells.size() >= depthLimit) rDL = true;
    return rDL;
  }
}
